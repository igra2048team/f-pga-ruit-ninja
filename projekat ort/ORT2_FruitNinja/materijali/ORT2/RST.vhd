-- clockDivider

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RST is
	port
	(
		clk			: in std_logic;
		rst	  		: out std_logic
	);

end entity;

architecture rtl of RST is	
begin

	process (clk)
		variable x:std_logic:='0';
	begin
	
		if (rising_edge(clk)) then
			if x='0' then
				x:='1';
				rst<='1';
			else
				rst<='0';
				end if;
		end if;
	end process;

end rtl;
