library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MP16X is

	generic
	(
		size : natural := 16
	);

	port 
	(
		XX0	: in std_logic_vector(size-1 downto 0);
		X1	: in std_logic_vector(size-1 downto 0);
		XX2	: in std_logic_vector(size-1 downto 0);
		XX3	: in std_logic_vector(size-1 downto 0);
		XX4	: in std_logic_vector(size-1 downto 0);
		XX5	: in std_logic_vector(size-1 downto 0);
		XX6	: in std_logic_vector(size-1 downto 0);
		XX7  : in std_logic_vector(size-1 downto 0);
		XX8  : in std_logic_vector(size-1 downto 0);
		XX9  : in std_logic_vector(size-1 downto 0);
		XX10 : in std_logic_vector(size-1 downto 0);
		XX11 : in std_logic_vector(size-1 downto 0);
		XX12 : in std_logic_vector(size-1 downto 0);
		XX13 : in std_logic_vector(size-1 downto 0);
		XX14 : in std_logic_vector(size-1 downto 0);
		XX15 : in std_logic_vector(size-1 downto 0);
		S : in std_logic_vector(3 downto 0);
		Y : out std_logic_vector(size-1 downto 0)
		
	);

end entity;

architecture rt1 of MP16X is
begin

	process (S,XX0,X1,XX2,XX3,XX4,XX5,XX6,XX7,XX8,XX9,XX10,XX11,XX12,XX13,XX14,XX15)
	begin
		case S is
			when "0000" => Y <= XX0;
			when "0001" => Y <= X1;
			when "0010" => Y <= XX2;
			when "0011" => Y <= XX3;
			when "0100" => Y <= XX4;
			when "0101" => Y <= XX5;
			when "0110" => Y <= XX6;
			when "0111" => Y <= XX7;
			when "1000" => Y <= XX8;
			when "1001" => Y <= XX9;
			when "1010" => Y <= XX10;
			when "1011" => Y <= XX11;
			when "1100" => Y <= XX12;
			when "1101" => Y <= XX13;
			when "1110" => Y <= XX14;
			when "1111" => Y <= XX15;
		end case;
	end process;
end rt1;
