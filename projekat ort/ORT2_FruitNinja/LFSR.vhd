 library ieee;
 use ieee.std_logic_1164.all;

entity lfsr is
   port (
   cout   :out std_logic_vector (7 downto 0);
	enable :in  std_logic;                   -- Enable counting
	clk    :in  std_logic;                   -- Input rlock
   reset  :in  std_logic                    -- Input reset
 );
end entity;

architecture rtl of lfsr is
   signal count           :std_logic_vector (7 downto 0);
   signal linear_feedback :std_logic;
 
begin
   linear_feedback <= not(count(5) xor count(3));


    process (clk, reset) begin
        if (reset = '1') then
            count <= (others=>'0');
        elsif (rising_edge(clk)) then
           if (enable = '1') then
                count <= (count(6) & count(5) & count(4) & count(3) 
                         & count(2) & count(1) & count(0) & 
                         linear_feedback);
								 end if;
       end if;
    end process;
    cout <= count;
end architecture;