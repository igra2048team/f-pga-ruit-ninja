library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity CD16X is
port (
enable :in std_logic; -- Enable for the encoder
encoder_in :in std_logic_vector (15 downto 0);-- 16-bit Input
binary_out :out std_logic_vector (3 downto 0) -- 4 bit binary Output

);
end entity;

architecture behavior of CD16X is
begin
process (enable, encoder_in)
variable x:integer;
 begin
--binary_out <= "0000";
x:=0;
if (enable = '1') then

	for i in encoder_in'low to encoder_in'high loop
		if(encoder_in(i)='1') then
			x:=i;
		end if;
	end loop;
end if;
binary_out<=std_logic_vector(to_unsigned(x,4));

end process;
end architecture;